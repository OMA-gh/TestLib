#pragma once

#include "Actor.h"

class Test : public Actor {
public:
	Test();

    virtual void prepare(const ActorInstance::CreateArg& arg, const Actor::DefaultParam& param) override;
	virtual void update() override;
private:
};