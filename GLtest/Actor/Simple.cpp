#include "Simple.h"
#include "../System/Manager.h"

Simple::Simple() : Actor() {
}

void Simple::prepare(const ActorInstance::CreateArg& arg, const Actor::DefaultParam& param) {
    Actor::prepare(arg, param);
}

void Simple::update() {
}

void Simple::reflectBody() {
    Actor::reflectBody();
    
    const glm::mat4 rotationMatrix = glm::mat4(
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, -1.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );
    mRotation = mRotation* rotationMatrix;

    const glm::vec3 d_pos(0.f, 0.4f, 0.f);
    mPosition += d_pos;
}