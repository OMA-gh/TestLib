#pragma once

#include "../Model/Model.h"
#include "../System/ActorInstance.h"

class Actor {
public:
    struct DefaultParam {
        glm::vec3 pos;
        glm::vec3 scale;
    };

public:
	Actor();

    virtual void prepare(const ActorInstance::CreateArg& arg, const DefaultParam& param);
    virtual void update() { printf("WARNING|基底アクターのupdateです。\n"); };
	virtual void render() const;
    virtual void renderCollision() const;

    virtual void reflectBody();

	void setModel(const Model* const model) {
		mModel = model;
	}
	const Model* const getModelPtr() {
		return mModel;
	}

	const glm::vec3& getPosition() const{
		return mPosition;
	}
	void setPosition(const glm::vec3& pos) {
		mPosition = pos;
	}
	const glm::vec3& getScale() const {
		return mScale;
	}
	void setScale(const glm::vec3& scale) {
		mScale = scale;
	}
	const glm::mat4& getRotation() const {
		return mRotation;
	}
	void setRotation(const glm::mat4& rotate) {
		mRotation = rotate;
	}
    const std::string& getName() const {
        return mName;
    }
    void setName(const std::string& name) {
        mName = name;
    }
    bool isThroughLight() {
        return mIsThroughLight;
    }

protected:
    void ConstructDynamicPhysicsActor(const physx::PxGeometry& geometry, physx::PxMaterial& material);
    void ConstructDynamicPhysicsActor(const physx::PxTransform& transform, const physx::PxGeometry& geometry, physx::PxMaterial& material);
    void ConstructStaticPhysicsActor(const physx::PxGeometry& geometry, physx::PxMaterial& material);
    void ConstructStaticPhysicsActor(const physx::PxTransform& transform, const physx::PxGeometry& geometry, physx::PxMaterial& material);
    void SetPhysicsActor(const ActorInstance::PhysicsArg& arg);

protected:
	glm::vec3 mPosition;
	glm::vec3 mScale;
	glm::mat4 mRotation;
    std::string mName;
    int mCount;
    bool mIsThroughLight;

	const Model* mModel;
    physx::PxRigidActor* mPhysicsActorPtr;
};