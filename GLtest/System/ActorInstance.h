#pragma once

#include "../Model/Model.h"
#include <glm\glm.hpp>
#include "Physics.h"

class ActorInstance {

public:

    struct PhysicsArg {
        Physics::PhysicsShape physicsShape;
        Physics::PhysicsType physicsType;
        glm::vec3 centerPos;
        glm::vec3 scale;
    };
    struct CreateArg {
        std::string actorType;
        std::string modelKey;
        PhysicsArg physicsArg;
    };

public:
    ActorInstance();
    ~ActorInstance();

public:
    const CreateArg& getParam() const {
        return mArg;
    }
    CreateArg* getParamPtr() {
        return &mArg;
    }

private:
    CreateArg mArg;
};
