#pragma once

#include <vector>
#include <unordered_map>
#include <memory>

#include "Singleton.h"

#include "ActorInstance.h"

// Actors
#include "../Actor/Actor.h"

class ActorMgr : public Singleton<ActorMgr> {
public:
    friend class Singleton<ActorMgr>;//Singletonでのインスタンス作成は許可

public:
    ActorMgr();
    ~ActorMgr();

    void preCalc();
    void calc();
    void postCalc();

    bool addActor(const std::string& actor_name, const std::string& key, const Actor::DefaultParam& param);
    bool addActorDynamic(const std::string& actor_name, const std::string& key, const Actor::DefaultParam& param);
    
    Actor* getActorPtr(std::string actor_name);
    const Actor& getActor(std::string actor_name);
    const std::unordered_map<std::string, std::unique_ptr<Actor>>& getActorArray() const {
        return mActorPtrArray;
    }
    bool ClearAll();

    /*
     * アクター定義を読み込んでインスタンスを作成していく
     */
    void LoadActorInstance();

    /*
     * アクタータイプに応じたアクタークラスを作成する
     */
    std::unique_ptr<Actor> CreateActorClass(const std::string& actor_type);

private:
    std::unordered_map<std::string, std::unique_ptr<ActorInstance>> mActorInstancePtrArray;
    std::unordered_map<std::string, std::unique_ptr<Actor>> mActorPtrArray;
    std::unordered_map<std::string, std::unique_ptr<Actor>> mAddRequestPtrArray;
};