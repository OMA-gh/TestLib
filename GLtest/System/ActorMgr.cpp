
#include "Macro.h"
#include "ModelMgr.h"

#include "ActorMgr.h"
#include "../Actor/Test.h"

#include "../Actor/Test.h"
#include "../Actor/Light.h"
#include "../Actor/Simple.h"

ActorMgr *ActorMgr::s_pInstance = nullptr;

ActorMgr::ActorMgr() {
};
ActorMgr::~ActorMgr() {
    ClearAll();
};

void ActorMgr::preCalc() {
    auto end = mAddRequestPtrArray.end();
    for (auto it = mAddRequestPtrArray.begin(); it != end; it++) {
        if (mActorPtrArray[it->first] == nullptr) {
            mActorPtrArray[it->first] = std::move(it->second);
        }
    }
    mAddRequestPtrArray.clear();
}

void ActorMgr::calc() {
    for (auto it = mActorPtrArray.begin(), end = mActorPtrArray.end(); it != end; it++) {
        (*it).second->update();
    }
}

void ActorMgr::postCalc() {
    for (auto it = mActorPtrArray.begin(), end = mActorPtrArray.end(); it != end; it++) {
        (*it).second->reflectBody();
    }
}

bool ActorMgr::addActor(const std::string& actor_name, const std::string& key, const Actor::DefaultParam& param) {
    if (mActorPtrArray[actor_name] == nullptr && mActorInstancePtrArray[key] != nullptr) {
        ActorInstance* actor_instance = mActorInstancePtrArray[key].get();
        std::unique_ptr<Actor> add_actor = CreateActorClass(actor_instance->getParam().actorType);
        add_actor->prepare(actor_instance->getParam(), param);
        add_actor->setModel(GET_INSTANCE(ModelMgr)->getModelPtr(actor_instance->getParam().modelKey));
        add_actor->setName(actor_name);
        mAddRequestPtrArray[actor_name] = std::move(add_actor);

        return true;
    }
    else {
        return false;
    }
}

bool ActorMgr::addActorDynamic(const std::string& actor_name, const std::string& key, const Actor::DefaultParam& param) {
    if (mActorPtrArray[actor_name] == nullptr && mActorInstancePtrArray[key] != nullptr) {
        ActorInstance* actor_instance = mActorInstancePtrArray[key].get();
        std::unique_ptr<Actor> add_actor = CreateActorClass(actor_instance->getParam().actorType);
        add_actor->prepare(actor_instance->getParam(), param);
        add_actor->setModel(GET_INSTANCE(ModelMgr)->getModelPtr(actor_instance->getParam().modelKey));
        add_actor->setName(actor_name);
        mActorPtrArray[actor_name] = std::move(add_actor);

        return true;
    }
    else {
        return false;
    }
}

const Actor& ActorMgr::getActor(std::string actor_name) {
    auto find = mActorPtrArray.find(actor_name);
    return *find->second.get();
}

Actor* ActorMgr::getActorPtr(std::string actor_name) {
    auto find = mActorPtrArray.find(actor_name);
    if (find == mActorPtrArray.end()) {
        printf("ERROR:actor is not exist [actor name:%s] @ActorMgr::getActorPtr()", actor_name.c_str());
        return nullptr;
    }
    else {
        return find->second.get();
    }
}

bool ActorMgr::ClearAll() {
    {
        for (auto it = mActorPtrArray.begin(), end = mActorPtrArray.end(); it != end; it++) {
            (*it).second.reset();
        }
        mActorPtrArray.clear();
    }
    {
        for (auto it = mAddRequestPtrArray.begin(), end = mAddRequestPtrArray.end(); it != end; it++) {
            (*it).second.reset();
        }
        mAddRequestPtrArray.clear();
    }
    {
        for (auto it = mActorInstancePtrArray.begin(), end = mActorInstancePtrArray.end(); it != end; it++) {
            (*it).second.reset();
        }
        mAddRequestPtrArray.clear();
    }
    return true;
}

void ActorMgr::LoadActorInstance() {
    // リソースからアクター定義を読み込んでインスタンスを作る

    {
        std::unique_ptr<ActorInstance> actor = std::make_unique<ActorInstance>();
        actor->getParamPtr()->actorType = "Simple";
        actor->getParamPtr()->modelKey = "bunny";
        actor->getParamPtr()->physicsArg.centerPos = glm::vec3(0.f, 5.f, 0.f);
        actor->getParamPtr()->physicsArg.scale = glm::vec3(1.f);
        actor->getParamPtr()->physicsArg.physicsShape = Physics::PhysicsShape::Box;
        actor->getParamPtr()->physicsArg.physicsType = Physics::PhysicsType::Dynamic;
        mActorInstancePtrArray["oma"] = std::move(actor);
    }
    {
        std::unique_ptr<ActorInstance> actor = std::make_unique<ActorInstance>();
        actor->getParamPtr()->actorType = "Light";
        actor->getParamPtr()->modelKey = "cube";
        actor->getParamPtr()->physicsArg.centerPos = glm::vec3(0.f);
        actor->getParamPtr()->physicsArg.scale = glm::vec3(0.f);
        actor->getParamPtr()->physicsArg.physicsShape = Physics::PhysicsShape::None;
        actor->getParamPtr()->physicsArg.physicsType = Physics::PhysicsType::Static;
        mActorInstancePtrArray["Light"] = std::move(actor);
    }
    {
        std::unique_ptr<ActorInstance> actor = std::make_unique<ActorInstance>();
        actor->getParamPtr()->actorType = "Test";
        actor->getParamPtr()->modelKey = "terrain";
        actor->getParamPtr()->physicsArg.centerPos = glm::vec3(0.5f, 0.5f, 0.5f);
        actor->getParamPtr()->physicsArg.scale = glm::vec3(20.0f, 0.001f, 20.0f);
        actor->getParamPtr()->physicsArg.physicsShape = Physics::PhysicsShape::Box;
        actor->getParamPtr()->physicsArg.physicsType = Physics::PhysicsType::Static;
        mActorInstancePtrArray["Terrain"] = std::move(actor);
    }
    {
        std::unique_ptr<ActorInstance> actor = std::make_unique<ActorInstance>();
        actor->getParamPtr()->actorType = "Simple";
        actor->getParamPtr()->modelKey = "cube";
        actor->getParamPtr()->physicsArg.centerPos = glm::vec3(0.5f, 0.5f, 0.5f);
        actor->getParamPtr()->physicsArg.scale = glm::vec3(1.0f, 1.9f, 1.0f);
        actor->getParamPtr()->physicsArg.physicsShape = Physics::PhysicsShape::Box;
        actor->getParamPtr()->physicsArg.physicsType = Physics::PhysicsType::Dynamic;
        mActorInstancePtrArray["Test"] = std::move(actor);
    }
}

std::unique_ptr<Actor> ActorMgr::CreateActorClass(const std::string& actor_type) {
    if (actor_type.compare("Light") == 0) {
        return std::make_unique<Light>();
    }
    else if (actor_type.compare("Test") == 0) {
        return std::make_unique<Test>();
    }
    else if (actor_type.compare("Simple") == 0) {
        return std::make_unique<Simple>();
    }
}